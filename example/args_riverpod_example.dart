import 'dart:io';

import 'package:args_riverpod/args_riverpod.dart';

import 'package:args/command_runner.dart';
import 'package:riverpod/riverpod.dart';

// CLI sample call:
// dart "./args_riverpod/example/args_riverpod_example.dart" --host server.com  --token my_token branch --input path/in-file.txt leaf --output path/out-file.txt

// CLI main & global options handling
//
void main(List<String> arguments) {
  ProviderCommandRunner("cli", "A super CLI.")
    ..setGlobalArgProcessor(GlobalOptionParser())
    ..addCommand(BranchCommand())
    ..argParser.addOption('host', help: 'Server host URL')
    ..argParser.addOption('token', help: 'Server access token')
    ..run(arguments);
}

class GlobalOptionParser extends ProviderArgsProcessor {
  @override
  Future<void> processArgs() async {
    print('GlobalOptionParser.processArgs called');
    ref.read(hostArgProvider.notifier).state = argResults?['host'];
    ref.read(tokenArgProvider.notifier).state = argResults?['token'];
  }
}

// Branch command & related options handling
//
class BranchCommand extends ProviderCommand {
  @override
  final name = "branch";
  @override
  final description = "branch command";

  BranchCommand() {
    argParser.addOption('input', help: 'input file path');
    addSubcommand(LeafCommand());
  }

  @override
  void processArgs() {
    print('BranchCommand.processArgs called');
    ref.read(inputArgProvider.notifier).state = argResults?['input'];
  }
}

// Leaf command, related options handling & command execution
//
class LeafCommand extends ProviderCommand {
  @override
  final name = "leaf";
  @override
  final description = "leaf command";

  LeafCommand() {
    argParser.addOption('output', help: 'output file path');
  }

  @override
  void processArgs() {
    print('LeafCommand.processArgs called');
    ref.read(ouputArgProvider.notifier).state = argResults?['output'];
  }

  @override
  void execute() {
    print('LeafCommand.execute called');
    ref.read(fooServiceProvider).doSomething();
  }
}

// Options providers
//
final hostArgProvider = StateProvider<String?>((ref) => null);
final tokenArgProvider = StateProvider<String?>((ref) => null);
final inputArgProvider = StateProvider<String?>((ref) => null);
final ouputArgProvider = StateProvider<String?>((ref) => null);
final fooServiceProvider = Provider<FooService>((ref) {
  return FooService(input: ref.watch(inputArgProvider), output: ref.watch(ouputArgProvider));
});

// Service
//
class FooService {
  FooService({
    required this.input,
    required this.output,
  });
  final String? input;
  final String? output;

  Future<void> doSomething() async {
    print('FooService.doSomething called');
    // Do the job
  }
}
