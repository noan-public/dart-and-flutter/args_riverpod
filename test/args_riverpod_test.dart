import 'package:args_riverpod/args_riverpod.dart';
import 'package:mockito/mockito.dart';
import 'package:test/test.dart';

void main() {
  group('Arguments processors', () {
    test('Command runner without global args processing', () {
      var argsProcessor = ProviderArgsProcessorSpy();
      ProviderCommandRunner("test", "test runner")
        ..argParser.addOption('option')
        ..run(['--option', 'test']);
      verifyNever(argsProcessor.processArgs());
    });

    test('Command runner with global args processing', () {
      var argsProcessor = ProviderArgsProcessorSpy();
      ProviderCommandRunner("test", "test runner")
        ..setGlobalArgProcessor(argsProcessor)
        ..argParser.addOption('option')
        ..run(['--option', 'test']);

      verify(argsProcessor.processArgs()).called(1);
    });

    test('Leaf command args processing', () {
      var cmd = ProviderCommandSpy('spy');
      ProviderCommandRunner("test", "test runner")
        ..addCommand(cmd)
        ..run(['spy', '--option', 'test']);

      expect(cmd.processArgsCallCount, 1);
      expect(cmd.executeCallCount, 1);
    });

    test('Leaf sub command args processing', () {
      var cmd = ProviderCommandSpy('spy');
      var subCmd = ProviderCommandSpy('run');
      cmd.addSubcommand(subCmd);
      ProviderCommandRunner("test", "test runner")
        ..addCommand(cmd)
        ..run(['spy', '--option', 'test', 'run', '--option', 'test']);

      expect(cmd.processArgsCallCount, 1);
      expect(cmd.executeCallCount, 0);
      expect(subCmd.processArgsCallCount, 1);
      expect(subCmd.executeCallCount, 1);
    });
  });

  group('Provider container', () {
    test('Arg processor has container when processed', () {
      var argsProcessor = ProviderArgsProcessorSpy();
      var runner = ProviderCommandRunner("test", "test runner")
        ..setGlobalArgProcessor(argsProcessor)
        ..argParser.addOption('option')
        ..run(['--option', 'test']);

      expect(argsProcessor.ref, isNotNull);
      expect(argsProcessor.ref, runner.ref);
    });

    test('Command has container when processed', () {
      var cmd = ProviderCommandSpy('spy');
      var runner = ProviderCommandRunner("test", "test runner")
        ..addCommand(cmd)
        ..run(['spy', '--option', 'test']);

      expect(cmd.ref, isNotNull);
      expect(cmd.ref, runner.ref);
    });

    test('Sub command has container when processed', () {
      var cmd = ProviderCommandSpy('spy');
      var subCmd = ProviderCommandSpy('run');
      cmd.addSubcommand(subCmd);
      var runner = ProviderCommandRunner("test", "test runner")
        ..addCommand(cmd)
        ..run(['spy', '--option', 'test', 'run', '--option', 'test']);

      expect(cmd.ref, isNotNull);
      expect(cmd.ref, runner.ref);
      expect(subCmd.ref, isNotNull);
      expect(subCmd.ref, runner.ref);
    });
  });
}

class ProviderArgsProcessorSpy extends Mock with ProviderArgsProcessor {}

class ProviderCommandSpy extends ProviderCommand {
  @override
  String name;
  @override
  final description = 'description';

  ProviderCommandSpy(this.name) {
    argParser.addOption('option');
  }

  @override
  void processArgs() {
    processArgsCallCount++;
  }

  @override
  void execute() {
    executeCallCount++;
  }

  int processArgsCallCount = 0;
  int executeCallCount = 0;

  void reset() {
    processArgsCallCount = 0;
    executeCallCount = 0;
  }
}
