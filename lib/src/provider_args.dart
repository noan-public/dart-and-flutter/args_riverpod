import 'dart:async';

import 'package:args/args.dart';
import 'package:args/command_runner.dart';
import 'package:meta/meta.dart';
import 'package:riverpod/riverpod.dart';

/// A class for invoking [Command]s based on raw command-line arguments.
/// It provides a root Riverpod container.
///
/// The type argument `T` represents the type returned by [Command.run] and
/// [CommandRunner.run]; it can be ommitted if you're not using the return
/// values.
class ProviderCommandRunner<T> extends CommandRunner<T> {
  ProviderContainer get ref => _ref;
  late final ProviderContainer _ref;
  ProviderArgsProcessor? globalArgsProcessor;

  ProviderCommandRunner(super.executableName, super.description,
      {List<Override> overrides = const [], List<ProviderObserver>? observers})
      : _ref = ProviderContainer(overrides: overrides, observers: observers);

  @override
  void addCommand(Command<T> command) {
    if (command is ProviderCommand) {
      (command as ProviderCommand)._ref = _ref;
    }
    super.addCommand(command);
  }

  void setGlobalArgProcessor(ProviderArgsProcessor handler) {
    handler._ref = ref;
    globalArgsProcessor = handler;
  }

  @override
  Future<T?> run(Iterable<String> args) {
    globalArgsProcessor?._argResults = parse(args);
    globalArgsProcessor?.processArgs();
    return super.run(args);
  }
}

/// A single command.
/// It provides the root Riverpod container.
///
/// A command is known as a "leaf command" if it has no subcommands and is meant
/// to be run. Leaf commands must override [execute] and can override [processArgs].
///
/// A command with subcommands is known as a "branch command" and cannot be executed
/// itself. It should call [addSubcommand] (often from the constructor) to
/// register subcommands.
/// Branch command can override [processArgs] to process their level arguments.
abstract class ProviderCommand<T> extends Command<T> implements ArgsProcessor {
  ProviderContainer get ref => _providerContainer!;

  ProviderContainer? _providerContainer;

  set _ref(ProviderContainer ref) {
    _providerContainer = ref;
    for (var subCommand in subcommands.values) {
      // Cannot be done be overriding addSubcommand because when adding sub-command,
      // addCommand of parent ProviderCommand has not yet been called
      // and thus ProviderContainer is null
      if (subCommand is ProviderCommand) {
        (subCommand as ProviderCommand)._providerContainer = ref;
      }
    }
  }

  bool _isLeaf() {
    return argResults?.command == null;
  }

  @nonVirtual
  @override
  FutureOr<T>? run() {
    FutureOr<T>? result;
    parent?.run(); // Recursive call to parent
    processArgs();
    if (_isLeaf()) result = execute();
    return result;
  }

  FutureOr<T>? execute() {
    if (_isLeaf()) {
      throw UnimplementedError('Leaf command $this must implement run().');
    }
    return null;
  }

  @override
  void processArgs() {
    /* Do nothing if not overriden */
  }
}

/// Arg handler is used to process global arguments.
/// It provides the root Riverpod container.
///
/// A arg handler is meant to be run a,d must override [processArgs].
abstract class ProviderArgsProcessor implements ArgsProcessor {
  ProviderContainer get ref => _ref;
  late final ProviderContainer _ref;

  /// The parsed argument results for this command.
  ///
  /// This will be `null` until just before [Command.run] is called.
  ArgResults? get argResults => _argResults;
  ArgResults? _argResults;
}

abstract class ArgsProcessor {
  /// Process arguments
  void processArgs();
}
